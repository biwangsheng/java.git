package com.biws.mapper;

import com.biws.model.TpmTestLog;

/**
 * @author ：biws
 * @date ：Created in 2020/12/19 21:56
 * @description：
 */

public interface TpmTestLogMapper {

    Long insert(TpmTestLog tpmTestLog);

}
