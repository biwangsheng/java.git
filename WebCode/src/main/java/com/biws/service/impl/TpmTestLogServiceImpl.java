package com.biws.service.impl;

import com.biws.mapper.TpmTestLogMapper;
import com.biws.model.TpmTestLog;
import com.biws.service.TpmTestLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ：biws
 * @date ：Created in 2020/12/19 21:59
 * @description：
 */

@Service
@Transactional
public class TpmTestLogServiceImpl implements TpmTestLogService {

    @Autowired
    private TpmTestLogMapper tpmTestLogMapper;

    @Override
    public Long ceshi(TpmTestLog tpmTestLog) {
        return tpmTestLogMapper.insert(tpmTestLog);
    }
}
