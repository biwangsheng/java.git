# java

## 介绍

提交自己在日常得学习生活中形成得代码模块，希望对大家有所帮助

不过也是一个基础小渣渣，有写的不好的地方还请各位指出改正，谢谢

## 软件架构

因为不涉及项目，都是一些demo级别得，后面我会去更新一些项目框架，因为现在公司得东西没有办法对外，全都是内网操作，就很烦

内容得话涉及Java和大数据吧，我主要是做大数据方向得，但是老大说我Java方向短板太大了，需要进行一个系统得提升，所以我这边才开始进行一些Java代码得学习，所以内容从基础到进阶，都会涉及一些，适合所有人

 **我的gzh：Java架构师联盟** ，更新内容主要以Java为主，会有一个大数据得体系，不定时更新
![输入图片说明](https://images.gitee.com/uploads/images/2020/1219/161225_c042c655_7624163.png "默认文件1590838906000.png")

## 安装教程

这是我现在在学习得过程中，使用得开发环境，对于新学者比较友好，大家可以参考

 **这两篇文章中会讲解配置jdk和tomcat以及centos7得使用** 

[我哭了！Centos6居然停止更新只能切换7，我要重新改变习惯了](https://mp.weixin.qq.com/s/NbcqhOycUWPqvDHOeXs3_A)

[为了SpringBoot提交Tomcat执行，我径然总结了这么多](https://mp.weixin.qq.com/s/GEByHrqZXEFcnsYeYf-5-A)

 **这两篇是配置git以及idea得** 

[被我玩坏的git：除了之前的工作、当网盘用，还能这么玩](https://mp.weixin.qq.com/s?__biz=Mzg2MTAwMTI3MA==&mid=2247486757&idx=1&sn=bffffb92528b2f516d577f98dfa5a09e&chksm=ce1c8e7cf96b076a2f5d802018a0019f754c18494ca9966f79a98b2a67f495918c9643bedaff&token=422712965&lang=zh_CN#rd)

[由GitLab用户切换引发的某程序员“震怒”，怒而开源项目源码](https://mp.weixin.qq.com/s?__biz=Mzg2MTAwMTI3MA==&mid=2247488284&idx=2&sn=cc79e9e7ce43c0679ab49e706e2e55b3&chksm=ce1c9045f96b195341fdd057ba4a2d1ea5a2d1278223e430e4b14bcb4df26f0f83943877fd3d&token=422712965&lang=zh_CN#rd)



暂时能想到的就这么多，后期会不断的更新进来，我觉得可以持续关注，哈哈哈哈
## 技术整理

### Java

暂时不知道更新哪一个文章，后面我在更新吧

### Linux

 **作为刚接触这个得朋友，建议你先看一下这篇文章呀** 

[Linux玩不明白是因为你没看这个，熬夜手敲命令整理相应文档](https://mp.weixin.qq.com/s?__biz=Mzg2MTAwMTI3MA==&mid=2247488451&idx=1&sn=7b047cac7471268ed9247abfc305c7d6&chksm=ce1c909af96b198c97cc77803c63a86c3e7476fdd936a2839fb4deb60e15d53f746dd643f396&token=422712965&lang=zh_CN#rd)

持续更新

### 大数据
#### 大数据文档

[手把手带你玩转大数据系列--hdfs三种搭建方式一文搞定](https://mp.weixin.qq.com/s?__biz=Mzg2MTAwMTI3MA==&mid=2247483841&idx=1&sn=c8bf703423834aa33b6c9811bffede5f&chksm=ce1c8298f96b0b8e9ecef9160d384b5f557def5c900c93d51472ad900ac5de10360051744437&scene=178&cur_album_id=1338094397266804741#rd)
#### 大数据代码
[大数据学习应用API代码](https://gitee.com/biwangsheng/BigData.git)

持续更新

## 使用说明

1. 因为是刚开始操作，所以有很多不足的地方，大家可以跟我提一下建议
2. 在我的gzh里面，我现在逐步的将文章整理到一些标签中，方便大家成体系得学习 ，嘿嘿嘿
3. 后面我会将gzh中写的比较好得文章每星期更新一次，可以关注gzh：Java架构师联盟，一般都会每天更新一次技术文，哈哈哈哈