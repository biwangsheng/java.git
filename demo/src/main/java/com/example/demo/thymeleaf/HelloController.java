package com.example.demo.thymeleaf;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author ：biws
 * @date ：Created in 2020/12/18 19:43
 * @description：在controller中定义跳转页面
 */

@Controller
public class HelloController {

    @RequestMapping("hello")
    @ResponseBody
    public String hello() {
        return "这是第二种搭建springboot方式";
    }

    @RequestMapping("testThymeleaf")
    public String testThymeleaf(Map map) {

        map.put("info", "这是从controller传到前端界面得数据");
        return "index";
    }

}
