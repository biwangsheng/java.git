package com.biws.TestDate;

import org.junit.jupiter.api.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 22:06
 * @description：测试全部date类方法
 */
public class allDateFunction {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        new allDateFunction().hi();
        new allDateFunction().hello();
    }

    // JUnit 测试单元
    // 1. 配置快捷键 alt + J
    // 2. 如果要运行某个 测试单元，就选中方法名或光标定位在方法名，在运行 Junit
    // 3. 如果不选，就运行，就把所有的测试单元都运行
    // 4.@Test,代表此方法是测试单元，可以单独运行测试


    @Test
    public void hi() {
        System.out.println("hi ");
    }

    @Test
    public void hello() {
        System.out.println("hello");
    }

    @Test
    public void testLocalDate() {
        // 获取当前日期（只包含日期，不包含时间）
        LocalDate date = LocalDate.now();
        System.out.println(date);

        // 获取日期的指定部分
        System.out.println("year:" + date.getYear());
        System.out.println("month:" + date.getMonth());
        System.out.println("day:" + date.getDayOfMonth());
        System.out.println("week:" + date.getDayOfWeek());

        // 根据指定的日期参数，创建LocalDate对象
        LocalDate of = LocalDate.of(2010, 3, 2);
        System.out.println(of);

    }

    // 测试LocalTime类
    @Test
    public void testLocalTime() {
        // 获取当前时间（只包含时间，不包含日期）
        LocalTime time = LocalTime.now();
        System.out.println(time);

        // 获取时间的指定部分
        System.out.println("hour:" + time.getHour());
        System.out.println("minute:" + time.getMinute());

        System.out.println("second:" + time.getSecond());
        System.out.println("nano:" + time.getNano());

        // 根据指定的时间参数，创建LocalTime对象
        LocalTime of = LocalTime.of(10, 20, 55);
        System.out.println(of);

    }

    // 测试LocalDateTime类

    @Test
    public void testLocalDateTime() { // 获取当前时间（包含时间+日期）

        LocalDateTime time = LocalDateTime.now();

        // 获取时间的指定部分 System.out.println("year:" + time.getYear());
        System.out.println("month:" + time.getMonthValue());
        System.out.println("day:" + time.getMonth());
        System.out.println("day:" + time.getDayOfMonth());
        System.out.println("hour:" + time.getHour());
        System.out.println("minute:" + time.getMinute());

        System.out.println("second:" + time.getSecond());
        System.out.println("nano:" + time.getNano());

        // 根据指定的时间参数，创建LocalTime对象
        LocalDateTime of = LocalDateTime.of(2020, 2, 2, 10, 20, 55);
        System.out.println(of);

    }

    @Test
    public void testMonthDay() {

        LocalDate birth = LocalDate.of(1994, 7, 14); // 生日
        MonthDay birthMonthDay = MonthDay.of(birth.getMonthValue(), birth.getDayOfMonth());

        LocalDate now = LocalDate.now(); // 当前日期
        MonthDay current = MonthDay.from(now);

        if (birthMonthDay.equals(current)) {
            System.out.println("今天生日");
        } else {
            System.out.println("今天不生日");
        }

    }

    // 判断是否为闰年
    @Test
    public void testIsLeapYear() {

        LocalDate now = LocalDate.now();

        System.out.println(now.isLeapYear());

    }
    // 测试增加日期的某个部分

    @Test
    public void testPlusDate() {

        LocalDate now = LocalDate.now(); // 日期
        // 3年前 的日期
        LocalDate plusYears = now.plusDays(-1);
        System.out.println(plusYears);

    }

    // 使用plus方法测试增加时间的某个部分
    // 时间范围判断
    @Test
    public void testPlusTime() {

        LocalTime now = LocalTime.now();// 时间

        LocalTime plusHours = now.plusSeconds(-500);

        System.out.println(plusHours);

    }

    // 使用minus方法测试查看一年前和一年后的日期

    @Test
    public void testMinusTime() {
        LocalDate now = LocalDate.now();

        LocalDate minus = now.minus(1, ChronoUnit.YEARS);

        // LocalDate minus2 = now.minusYears(1);
        System.out.println(minus);

    }

    // 测试时间戳类：Instant ，相当于以前的Date类
    @Test
    public void testInstant() {
        Instant now = Instant.now();
        System.out.println(now);

        // 与Date类的转换
        Date date = Date.from(now);
        System.out.println(date);

        Instant instant = date.toInstant();

        System.out.println(instant);
    }

    // 格式转换
    @Test
    public void testDateTimeFormatter() {
        DateTimeFormatter pattern = DateTimeFormatter.ofPattern("MM-dd yyyy HH:mm:ss");

        // 将字符串转换成日期
        LocalDateTime parse = LocalDateTime.parse("03-03 2017 08:40:50", pattern);
        System.out.println(parse);

        // 将日期转换成字符串
        //LocalDateTime parse = LocalDateTime.now();

        String format = pattern.format(parse);
        System.out.println(format);
    }
}
