package com.biws.string;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:27
 * @description：String_StringBuffer_StringBuilder对比
 */
public class String_StringBuffer_StringBuilder {
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        String text = ""; //字符串
        long startTime = 0L;
        long endTime = 0L;
        StringBuffer buffer = new StringBuffer("");//StringBuffer
        StringBuilder builder = new StringBuilder("");//StringBuilder

        startTime = System.currentTimeMillis();
        for (int i = 0; i < 80000; i++) {
            buffer.append(String.valueOf(i));
        }
        endTime = System.currentTimeMillis();
        System.out.println("StringBuffer的执行时间：" + (endTime - startTime));



        startTime = System.currentTimeMillis();
        for (int i = 0; i < 80000; i++) {
            builder.append(String.valueOf(i));
        }
        endTime = System.currentTimeMillis();
        System.out.println("StringBuilder的执行时间：" + (endTime - startTime));



        startTime = System.currentTimeMillis();
        for (int i = 0; i < 80000; i++) {
            text = text + i;
        }
        endTime = System.currentTimeMillis();
        System.out.println("String的执行时间：" + (endTime - startTime));

    }
}
