package com.biws.string;

import java.util.Scanner;
import java.lang.StringBuffer;
/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:13
 * @description：stringBuffer测试类:求打印效果示例 7777123567.59=> 7,777,123,567.59 ；123,564.59 <= 123456.59
 */
class StringBufferTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String in = scanner.next();
        StringBuffer sb = new StringBuffer(in);
        int index = sb.indexOf(".");
        if (index != -1) {
            for (int i = index - 3; i > 0; i -= 3) {    //直接先减掉一个3，就定位到需要插入逗号的位置
                //if (i != index) {
                sb.insert(i, ",");
                //}
            }
        } else {
            for (int i = 0; i < sb.length(); i += 3) {
                if (i != 0) {
                    sb.insert(i, ",");
                }
            }
        }
        System.out.println(sb);
    }


}
