package com.biws.bignum;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 22:03
 * @description：
 */
public class test {
    public static void main(String[] args) {
        BigInteger i1 = new BigInteger("1234567890");
        BigInteger i2 = new BigInteger("200");
        // 2.调用常见的运算方法
        // System.out.println(b1+b2); 不能使用 这样的 + 方法运行
        // 并且，add 这些方法只能是大数于大数相加，BigInteger.add(BigInteger);
        System.out.println(i1.add(i2));// 加
        System.out.println(i1.subtract(i2));// 减
        System.out.println(i1.multiply(i2));// 乘
        System.out.println(i1.divide(i2));// 除

        BigDecimal b1 = new BigDecimal("1234567890.567");
        BigDecimal b2 = new BigDecimal("123");
        // 2.调用常见的运算方法
        // System.out.println(b1+b2); 不能使用 + 号运算..
        // 并且，add 这些方法只能是大数于大数相加，BigDecimal.add(BigDecimal);
        System.out.println(b1.add(b2));// 加
        System.out.println(b1.subtract(b2));// 减
        System.out.println(b1.multiply(b2));// 乘
        //后面这个 BigDecimal.ROUND_CEILING 需要指定，是精度
        //没有这个参数，则会提示:错误
        System.out.println(b1.divide(b2, BigDecimal.ROUND_CEILING));// 除
    }
}
