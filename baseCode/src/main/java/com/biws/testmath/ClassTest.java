package com.biws.testmath;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:42
 * @description：测试math类
 */
public class ClassTest {
    public static void main(String[] args) {
        //1.abs 绝对值
        int abs = Math.abs(9);
        System.out.println(abs);
        //2.pow 求幂
        double pow = Math.pow(-3.5, 4);
        System.out.println(pow);
        //3.ceil 向上取整,返回>=该参数的最小整数;
        double ceil = Math.ceil(-3.0001);
        System.out.println(ceil);
        //4.floor 向下取整，返回<=该参数的最大整数
        double floor = Math.floor(-4.999);
        System.out.println(floor);
        //5.round 四舍五入  Math.floor(该参数+0.5)
        long round = Math.round(-5.001);
        System.out.println(round);
        //6.sqrt 求开方
        double sqrt = Math.sqrt(-9.0);
        System.out.println(sqrt);
        //7.random 返回随机数【0——1)
        //[a-b]:int num = (int)(Math.random()*（b-a+1）+a)
        double random = Math.random();
        System.out.println(random);

        //小技巧：获取一个 a-b 之间的一个随机整数
        int a = (int)(Math.random()*(15-7+1)+7);
        System.out.println(a);
        /*
         * 理解：
         *  1.Math.random() 是 [0，1)的随机数
         *  2.(Math.random()*(15-7+1) 就是[0，9)
         *  3.Math.random()*(15-7+1)+7 就是[7，16)
         *  4.(int)取整就是 [7,15] ，即[a,b]之间的随机整数
         */
    }
}
