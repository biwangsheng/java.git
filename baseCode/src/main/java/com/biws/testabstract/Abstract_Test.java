package com.biws.testabstract;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:14
 * @description：抽象类测试
 */
public class Abstract_Test {

        public static void main(String[] args) {

            Cat cat = new Cat("小花猫");
            cat.eat();
        }

    }


    abstract class Animal { //抽象类
        private String name;

        public Animal(String name) {
            super();
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        //eat , 抽象方法
        public abstract void eat();
    }

    //解读
//1. 当一个类继承了抽象类,就要把抽象类的所有抽象方法实现
//2. 所谓方法实现，指的是 把方法体写出, 方法体是空，也可以.
    class Cat extends Animal {
        public Cat(String name) {
            super(name);
            // TODO Auto-generated constructor stub
        }

        public void eat() {
            System.out.println(getName() +  " 爱吃 <・)))><<");
        }
    }

