package com.biws.testabstract;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:24
 * @description：模板设计模式
 */
public class Abstract_Template {
    public static void main(String[] args) {
        Template sub = new Sub();
        sub.caleTimes(); //实际是调用了Template中的caleTimes方法

        Template subStringB = new SubStringB();
        subStringB.caleTimes();

        //这里可以看到 StringBuffer在拼接字符串时，远远优于String拼接的效率
    }
}

abstract class Template{ //抽象类
    public abstract void code(); //抽象方法
    public void caleTimes(){ //  统计耗时多久是确定
        //统计当前时间距离 1970-1-1 0:0:0 的时间差，单位ms
        long start = System.currentTimeMillis();
        code(); //这里的code在调用时，就是指向子类中已经重写实现了的code
        long end = System.currentTimeMillis();
        System.out.println("耗时："+(end-start));
    }
}

class Sub extends Template{

    @Override
    public void code() {
        String x = "";
        for(int i = 0;i < 10000 ; i++) {    //拼接1W个hello 看处理时间
            x += "hello" + i;
        }
    }
}

class SubStringB extends Template{
    @Override
    public void code() {
        StringBuffer stringBuffer = new StringBuffer();
        for(int i = 0;i < 10000 ; i++) {    //拼接1W个hello 看处理时间
            stringBuffer.append("hello" + i);
        }
    }
}
