package com.biws.testabstract;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:17
 * @description：多态在抽象类中的实现
 */
public class AbstractPolArray {

    public static void main(String[] args) {
        //抽象类不可以实例化，但可以使用多态数组
        Animal1[] animal1 = new Animal1[2];

        animal1[0] = new Dog1("小黑狗");
        animal1[1] = new Cat1("小花猫");

        //多态数组的使用
        for (int i = 0; i < animal1.length; i++) {
            show(animal1[i]);
        }
    }

    //这里不用担心会传入一个Animal类型的实例，因为Animal不能实例化
    //编译器不会通过，所以只会传入Animal的子类实例
    public static void show(Animal1 a) {
        a.eat();    //多态的使用

        if(a instanceof Dog1) {
            ((Dog1)a).watch();
        }else if(a instanceof Cat1) {
            ((Cat1)a).catchMouse();
        }
    }
}

abstract class Animal1{
    private String name;

    public Animal1(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //动物都有eat的动作，但我们并不知道每一个动物具体怎么样eat
    //所以这里通过抽象提供了eat方法，需要子类来实现
    public abstract void eat();
}

class Dog1 extends Animal1{
    public Dog1(String name) {
        super(name);
    }

    @Override
    public void eat() {
        System.out.println(getName() + "啃骨头......");
    }

    public void watch() {
        System.out.println(getName() + "守家.....");
    }
}

class Cat1 extends Animal1{
    public Cat1(String name) {
        super(name);
    }

    @Override
    public void eat() {
        System.out.println(getName() + "吃鱼......");
    }

    public void catchMouse(){
        System.out.println(getName() + "抓老鼠.....");
    }
}
