package com.biws.testarray;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:46
 * @description：重写array中的排序方法
 */
public class overwriteArraySort {
    @SuppressWarnings("unchecked")
    public static void sort(Integer[] arr, Comparator c) {
        Integer temp = 0;// 自动装箱
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (c.compare(arr[j] , arr[j + 1]) > 0) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        Integer[] arr = { 35, 25, 11, 32, 98, 22 };
//      MyArrays.sort(arr);//不添加 Comparator接口对象为参数，就是简单的冒泡排序方法
        System.out.println(Arrays.toString(arr));
        //添加匿名内部类对象为参数，就可以改变排序方式。用此方法可以很灵活的使用排序。
       overwriteArraySort.sort(arr, new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                //通过返回值的正负来控制，升序还是降序
                //只有当返回值为1时，才会发生交换。例如这里，o1 < o2时返回1 ，进行交换
                //也就是需要前面的数，比后面的数大，即降序
                if (o1 < o2) {
                    return -1;
                } else if (o1 > o2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        System.out.println(Arrays.toString(arr));
    }

}
