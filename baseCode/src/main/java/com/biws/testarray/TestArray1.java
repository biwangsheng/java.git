package com.biws.testarray;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 21:44
 * @description：Array中常用排序方法
 */
public class TestArray1 {
    public static void main(String[] args) {
        Integer[] arr = { 25, 35, 11, 32, 98, 22 };
//      Arrays.sort(arr);   //默认小到大排序
        System.out.println(Arrays.toString(arr));

        // 使用匿名内部类重写compare方法，实现从大到小排序
        Arrays.sort(arr, new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 > o2) {
                    return -1;
                } else if (o1 < o2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        System.out.println(Arrays.toString(arr));
    }
}
