package com.biws.testarray;

import java.util.Arrays;
import java.util.List;

/**
 * @author ：biws
 * @date ：Created in 2020/12/22 22:01
 * @description：Arrays查找binarySearch方法及其他常用方法
 */
public class TestArray2 {
    public static void main(String[] args) {
        //随机生成数组，并排序
        Integer[] arr = new Integer[10];
//      for (int i = 0; i < arr.length; i++) {
//          arr[i] = (int)(Math.random()*100) + 1 ;
//      }
//      Arrays.sort(arr);
        arr = new Integer[]{16, 28, 41, 51, 62, 67, 67, 86, 90, 93};
        System.out.println(Arrays.toString(arr));

        // binarySearch 通过二分搜索法进行查找，要求必须排好序
        int index = Arrays.binarySearch(arr, 55);   //返回 -5 (55在51和62之间)
        //找到则返回下标，若没有找到则返回-(low + 1)。即此数在数组中应该在的位置的下标 + 1，例：
        //{1,3,5,9,10},此数组中找2，没有找到则返回-2，因为2本来在1和3的中间。下标为1再+1，就是-2
        System.out.println(index);


        // copyOf 数组元素的复制,参数列表：目标数组，需拷贝元素个数(若超过，使用null填充，若<0，报错)
        Integer[] newArr = Arrays.copyOf(arr, arr.length - 5);
        Integer[] newArr2 = Arrays.copyOf(arr, arr.length + 5);
        System.out.println(Arrays.toString(newArr));
        System.out.println(Arrays.toString(newArr2));

        // fill 数组元素的填充,将数组中的所有元素填充为所指定的内容
        Integer[] num = new Integer[] { 9, 3, 2 };
        Arrays.fill(num, 99);
        System.out.println(Arrays.toString(num));


        //equals 比较两个数组元素内容是否完全一致
        Integer[] array = new Integer[]{1,2,3};
        Integer[] array2 = new Integer[]{1,2,3};
        boolean equals = Arrays.equals(array, array2);
        System.out.println(equals);

        //asList 将一组值，转换成list
        List<Integer> asList = Arrays.asList(2,3,4,5,6,1);
        System.out.println("asList=" + asList);

    }
}
