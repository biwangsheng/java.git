package com.syn;

/**
 * @author ：biws
 * @date ：Created in 2020/12/21 22:27
 * @description：两个线程同时访问同一个对象的同步方法
 */

public class Condition1 implements Runnable {

    private static Condition1 instance = new Condition1();

    @Override
    public void run() {
        method();
    }

    //关键：synchronized可以保证此方法被顺序执行，线程1执行完4秒钟后，线程2再执行4秒。不加synchronized，线程1和线程2将同时执行
    private synchronized void method() {
        System.out.println("线程：" + Thread.currentThread().getName() + "，运行开始");
        try {
            //模拟执行一段操作，耗时4秒钟
            Thread.sleep(4000);
            System.out.println("线程：" + Thread.currentThread().getName() + "，运行结束");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // 模拟：同一个对象下，两个线程，同步执行一个方法（串行执行则为线程安全，并行执行，则为线程不安全，）
        Thread thread1 = new Thread(instance);
        Thread thread2 = new Thread(instance);
        thread1.start();
        thread2.start();
        while (thread1.isAlive() || thread2.isAlive()) {

        }
        System.out.println("测试结束");
    }
}
